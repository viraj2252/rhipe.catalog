﻿using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Rhipe.Catalog.Base.DomainObjects;
using Rhipe.Catalog.Base.Helpers;
using Rhipe.Catalog.Base.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Rhipe.Catalog.VendorOne.Services
{
    public class CatalogService: ICatalogService
    {
        private readonly ILogger _logger;
        private readonly IConnectionSetting _connectionSetting;
        private readonly HttpClient _httpClient;

        public CatalogService(ILogger<ICatalogService> logger, IConnectionSetting connectionSettings)
        {
            _logger = logger;
            _connectionSetting = connectionSettings;
            _logger.LogInformation("Initiaslize CatalogService");

            _httpClient = new HttpClient();
            _httpClient.DefaultRequestHeaders.Add("api-key", connectionSettings.APIKey);
        }

        public virtual async Task<Base.DomainObjects.Catalog> GetCatalog()
        {
            Base.DomainObjects.Catalog catalog = null;
            try
            {
                var res = await _httpClient.GetAsync(_connectionSetting.BaseURL + $"Products");

                if (!res.IsSuccessStatusCode)
                {
                    var err = await res.Content.ReadAsStringAsync();
                    _logger.LogError($"Required 200 but got {res.StatusCode}: {err}");
                    throw new BadHttpResponse($"Expected 200 but received {res.StatusCode}");
                }

                var contentStr = await res.Content.ReadAsStringAsync();
                var prodList = JsonConvert.DeserializeObject<ICollection<dynamic>>(contentStr).Select(s => 
                    new Product(s.productId.ToString(), 
                        s.name.ToString(), 
                        s.description.ToString(), 
                        decimal.Parse(s.unitPrice.ToString()), 
                        (s.maximumQuantity != null ? int.Parse(s.maximumQuantity.ToString()) : 0), 
                        20.0))
                    .ToList<Product>();
                catalog = new Base.DomainObjects.Catalog { Products = prodList };
            }
            catch (Exception ex)
            {

                _logger.LogError($"Error occurred while getting product data", ex);
            }

            return catalog;
        }
    }
}
