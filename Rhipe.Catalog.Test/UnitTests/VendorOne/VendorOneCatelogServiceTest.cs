﻿using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Rhipe.Catalog.Base.DomainObjects;
using Rhipe.Catalog.Base.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Rhipe.Catalog.Test.UnitTests.VendorOne
{
    [TestClass]
    public class VendorOneCatelogServiceTest
    {
        private readonly Mock<ILogger<ICatalogService>> _mockLogger;
        private readonly Mock<IConnectionSetting> _connectionSetting;

        public VendorOneCatelogServiceTest()
        {
            _mockLogger = new Mock<ILogger<ICatalogService>>();
            _connectionSetting = new Mock<IConnectionSetting>();
        }

        [TestMethod]
        public void Initialize_CatelogService_should_return_instance()
        {
            var catalogService = new VendorOneCatalogService(_mockLogger.Object, _connectionSetting.Object);
            

            Assert.IsNotNull(catalogService);
        }

        [TestMethod]
        public void GetCatalog_should_return_catelog_objuect_with_3_products()
        {
            var catalogService = new VendorOneCatalogService(_mockLogger.Object, _connectionSetting.Object);
            var catalog = catalogService.GetCatalog().GetAwaiter().GetResult();
            Assert.IsNotNull(catalog);
            Assert.IsNotNull(catalog.Products);
            Assert.IsTrue(catalog.Products.Count == 3);
        }
    }

    class VendorOneCatalogService : Catalog.VendorOne.Services.CatalogService
    {
        public VendorOneCatalogService(ILogger<ICatalogService> logger, IConnectionSetting connectionSetting) : base(logger, connectionSetting)
        { }

        public override async Task<Base.DomainObjects.Catalog> GetCatalog()
        {
            return new Base.DomainObjects.Catalog
            {
                Products = new List<Product> {
                    new Product("id1", "prod 1", "description one", 15.60M, 1000, 10.0),
                    new Product("id2", "prod 2", "description Two", 134.60M, 70, 20.0),
                    new Product("id3", "prod 3", "description Three", 120M, 1000, 10.0)
                }
            };
        }
    }
}
