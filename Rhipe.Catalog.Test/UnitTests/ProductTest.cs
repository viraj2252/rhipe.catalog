﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhipe.Catalog.Base.DomainObjects;
using System;
using System.Collections.Generic;
using System.Text;

namespace Rhipe.Catalog.Test.UnitTests
{
    [TestClass]
    public class ProductTest
    {
        [TestMethod]
        public void Initialize_product_should_return_instance()
        {
            var product = new Product("id1", "product1", "desc1", 10.50M, 10, 20.0);

            Assert.IsNotNull(product);
            Assert.AreEqual("id1", product.ProductId);
            Assert.AreEqual(10.50M, product.UnitPrice);
        }

        [TestMethod]
        public void GetMarkup_should_return_100_for_basePrice_100_with_markup_0_percent()
        {
            var expectedMarkupPrice = 100M;
            var product = new Product("id1", "product1", "desc1", 100M, 10, 0.0);

            decimal markupPriceRes = product.GetMarkupPrice();
            Assert.AreEqual(expectedMarkupPrice, markupPriceRes);
        }

        [TestMethod]
        public void GetMarkup_should_return_120_for_basePrice_100_with_markup_20_percent()
        {
            var expectedMarkupPrice = 120M;
            var product = new Product("id1", "product1", "desc1", 100M, 10, 20.0);

            decimal markupPriceRes = product.GetMarkupPrice();
            Assert.AreEqual(expectedMarkupPrice, markupPriceRes);
        }

        [TestMethod]
        public void MarkupPrice_should_return_120_for_basePrice_100_with_markup_20_percent()
        {
            var expectedMarkupPrice = 120M;
            var product = new Product("id1", "product1", "desc1", 100M, 10, 20.0);

            decimal markupPriceRes = product.MarkupPrice;
            Assert.AreEqual(expectedMarkupPrice, markupPriceRes);
        }
    
    }
}
