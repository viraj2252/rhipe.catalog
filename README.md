

This test project demonstrate the Basic application architecture for the code challenge. It has been designed such a way it could easily convert to microservices.

## Main Projects 

### Rhipe.Catalog.Base
This project contains all Base domain Objects, Interfaces, Base dependencies.

### Rhipe.Catalog.VendorOne
This is a project (Micro-service) which will communicate with its own vendor API and query all products available
Each vendor will have it's own project to communicate

### Rhipe.Catalog.Web
Internal API, client code(React/Redux) contain in this project

## Architecture

I have tried to make project follow micro-services architecture. Due to limited time, I have removed most of them(Like event-driven communication, etc...). At the moment Web application dependency is configured to call ICatalogService implementation of Rhipe.Catalog.VendorOne. 

In a real world application. Web application will broadcast an event to all subscribed Rhipe.Catalog.VendorX and get responses, consolidate and send back to front end through websocket. 


I have added basic Test project to demonstrate testing and did not add full range of Testing due to limited time

