﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Rhipe.Catalog.Base.Helpers
{
    public class BadHttpResponse : Exception
    {
        public BadHttpResponse(string message) : base(message) { }
    }
}
