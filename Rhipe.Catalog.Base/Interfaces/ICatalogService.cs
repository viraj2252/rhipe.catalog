﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Rhipe.Catalog.Base.DomainObjects;

namespace Rhipe.Catalog.Base.Interfaces
{
    public interface ICatalogService
    {
        Task<Base.DomainObjects.Catalog> GetCatalog();
    }
}
