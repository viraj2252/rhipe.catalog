﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Rhipe.Catalog.Base.Interfaces
{
    public interface IConnectionSetting
    {
        string BaseURL { get; set; }
        string APIKey { get; set; }
    }
}
