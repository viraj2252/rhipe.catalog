﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Rhipe.Catalog.Base.DomainObjects
{
    public class Product: Entity
    {
        public string ProductId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal UnitPrice { get; set; }
        public int? MaximumQuantity { get; set; }
        public decimal MarkupPrice => GetMarkupPrice();

        private double markupPercentage;

        public Product(string id, string name, string desctiption, decimal unitPrice, int? maxQuantity, double markupPercent)
        {
            ProductId = id;
            Name = name;
            Description = desctiption;
            UnitPrice = unitPrice;
            MaximumQuantity = maxQuantity;
            markupPercentage = markupPercent;
        }

        public decimal GetMarkupPrice()
        {
            var markupPrice = (markupPercentage == 0.0) ? UnitPrice : (UnitPrice + (UnitPrice * Convert.ToDecimal(markupPercentage) / 100M));

            return markupPrice;
        }
    }
}
