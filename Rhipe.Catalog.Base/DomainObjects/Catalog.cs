﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Rhipe.Catalog.Base.DomainObjects
{
    public class Catalog
    {
        public ICollection<Product> Products { get; set; }
    }
}
