﻿using Rhipe.Catalog.Base.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Rhipe.Catalog.Base.BaseModels
{
    public class ConnectionSettings : IConnectionSetting
    {
        public string BaseURL { get; set; }
        public string APIKey { get; set; }
    }
}
