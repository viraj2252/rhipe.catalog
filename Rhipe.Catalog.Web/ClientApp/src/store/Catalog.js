﻿const requestCatalogType = 'REQUEST_CATALOG';
const receiveCatalogType = 'RECEIVE_CATALOG';
const initialState = { products: [], isLoading: false };

export const actionCreators = {
    requestProductCatalog: startDateIndex => async (dispatch, getState) => {
        if (startDateIndex === getState().productCatalog.startDateIndex) {
            // Don't issue a duplicate request (we already have or are loading the requested data)
            return;
        }

        dispatch({ type: requestCatalogType, startDateIndex });

        const url = `api/catalog?startDateIndex=${startDateIndex}`;
        const response = await fetch(url);
        const catalog = await response.json();

        dispatch({ type: receiveCatalogType, startDateIndex, catalog });
    }
};

export const reducer = (state, action) => {
    state = state || initialState;

    if (action.type === requestCatalogType) {
        return {
            ...state,
            startDateIndex: action.startDateIndex,
            isLoading: true
        };
    }

    if (action.type === receiveCatalogType) {
        return {
            ...state,
            startDateIndex: action.startDateIndex,
            products: action.catalog.products,
            isLoading: false
        };
    }

    return state;
};
