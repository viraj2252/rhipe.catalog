import React from 'react';
import { connect } from 'react-redux';

const Home = props => (
  <div>
    <div class="stackedit__html">
      <p>This test project demonstrate the Basic application architecture for the code challenge. It has been designed
    such a way it could easily convert to microservices.</p>
      <h2 id="main-projects">Main Projects</h2>
      <h3 id="rhipe.catalog.base">Rhipe.Catalog.Base</h3>
      <p>This project contains all Base domain Objects, Interfaces, Base dependencies.</p>
      <h3 id="rhipe.catalog.vendorone">Rhipe.Catalog.VendorOne</h3>
      <p>This is a project (Micro-service) which will communicate with its own vendor API and query all products
    available<br />
        Each vendor will have it’s own project to communicate</p>
      <h3 id="rhipe.catalog.web"><a href="http://Rhipe.Catalog.Web">Rhipe.Catalog.Web</a></h3>
      <p>Internal API, client code(React/Redux) contain in this project</p>
      <h2 id="architecture">Architecture</h2>
      <p>I have tried to make project follow micro-services architecture. Due to limited time, I have removed most of
        them(Like event-driven communication, etc…). At the moment Web application dependency is configured to call
    ICatalogService implementation of Rhipe.Catalog.VendorOne.</p>
      <p>In a real world application. Web application will broadcast an event to all subscribed Rhipe.Catalog.VendorX and
    get responses, consolidate and send back to front end through websocket.</p>
      <p>I have added basic Test project to demonstrate testing and did not add full range of Testing due to limited time
  </p>
    </div>
  </div>
);

export default connect()(Home);
