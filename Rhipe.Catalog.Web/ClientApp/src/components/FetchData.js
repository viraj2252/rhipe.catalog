import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { actionCreators } from '../store/Catalog';

class FetchData extends Component {
  componentDidMount() {
    // This method is called when the component is first added to the document
    this.ensureDataFetched();
  }

  componentDidUpdate() {
    // This method is called when the route parameters change
    this.ensureDataFetched();
  }

  ensureDataFetched() {
    const startDateIndex = parseInt(this.props.match.params.startDateIndex, 10) || 0;
      this.props.requestProductCatalog(startDateIndex);
  }

  render() {
    return (
      <div>
        <h1>Product Catalog</h1>
        {renderForecastsTable(this.props)}        
      </div>
    );
  }
}

function renderForecastsTable(props) {
  return (
    <table className='table table-striped'>
      <thead>
        <tr>
          <th>Product Id</th>
          <th>Name</th>
          <th>Description</th>
          <th>Unit Price</th>
          <th>Mark-up Price</th>
        </tr>
      </thead>
      <tbody>
       {props.products.map(prod =>
           <tr>
            <td>{prod.productId}</td>
            <td>{prod.name}</td>
            <td>{prod.description}</td>
            <td>{prod.unitPrice}</td>
            <td>{prod.summary}</td>
            <td>{prod.markupPrice}</td>
          </tr>
        )}
      </tbody>
    </table>
  );
}

function renderPagination(props) {
  const prevStartDateIndex = (props.startDateIndex || 0) - 5;
  const nextStartDateIndex = (props.startDateIndex || 0) + 5;

  return <p className='clearfix text-center'>
    <Link className='btn btn-default pull-left' to={`/fetch-data/${prevStartDateIndex}`}>Previous</Link>
    <Link className='btn btn-default pull-right' to={`/fetch-data/${nextStartDateIndex}`}>Next</Link>
    {props.isLoading ? <span>Loading...</span> : []}
  </p>;
}

export default connect(
    state => state.productCatalog,
  dispatch => bindActionCreators(actionCreators, dispatch)
)(FetchData);
