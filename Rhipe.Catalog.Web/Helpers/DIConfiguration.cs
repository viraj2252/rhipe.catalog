﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using NLog.Extensions.Logging;
using Rhipe.Catalog.Base.BaseModels;
using Rhipe.Catalog.Base.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Rhipe.Catalog.Web.Helpers
{
    public static class DIConfiguration
    {
        public static void ConfigureServices(IServiceCollection services)
        {
            var environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            IConnectionSetting connectionSettings = new ConnectionSettings();

            //Set configurations
            var di = Directory.GetCurrentDirectory();
            var configBuilder = new Microsoft.Extensions.Configuration.ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: false)
                .AddJsonFile($"appsettings.{environmentName}.json", optional: true, reloadOnChange: false)
                .AddEnvironmentVariables();
            var configuration = configBuilder.Build();

            //Assign API settings
            connectionSettings.APIKey = configuration.GetSection("APIConfig")["APIKey"];
            connectionSettings.BaseURL = configuration.GetSection("APIConfig")["BaseURL"];

            services.AddSingleton(connectionSettings);
            services.AddSingleton<ILoggerFactory, LoggerFactory>();
            services.AddTransient<ICatalogService, VendorOne.Services.CatalogService>();

            var serviceProvider = services.BuildServiceProvider();

            var loggerFactory = serviceProvider.GetRequiredService<ILoggerFactory>();
            loggerFactory.AddNLog(new NLogProviderOptions { CaptureMessageTemplates = true, CaptureMessageProperties = true });
        }
    }
}
